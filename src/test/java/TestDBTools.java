import java.util.List;

import org.junit.Test;

import biz.webgate.education.business.Person;
import biz.webgate.education.db.DBConnector;

public class TestDBTools {
	@Test
	public void testGetPerson(){
		DBConnector con = new DBConnector();
		List<Person> plist =  con.getPersons();
		assert(plist.size()>0);
	}
}
