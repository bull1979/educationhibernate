package biz.webgate.education.db;

import java.util.List;

import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;

import biz.webgate.education.business.Filmberuf;
import biz.webgate.education.business.Land;
import biz.webgate.education.business.Person;

public class DBConnector {

	public List<Person> getPersons() {
		SessionFactory sessionFactory = getSessionFactory();
		Transaction tx = null;
		List<Person> result=null;
		Session session = null;
		System.out.println("sch�ne");
		try {
			session = sessionFactory.openSession();
			tx = session.beginTransaction();
			SQLQuery query = session.createSQLQuery("select * from person");
			//result = session.createQuery("from Person").list();
			List<Object[]> ret = query.list();
			System.out.println("Anzahl: " + ret.size());
			for(Object[] o : ret){
				System.out.println("anzahl Felder: " + o.length);
			}
			
			List<Person> plist = session.createQuery("from Person").list();
			Person p = plist.get(0);
			System.out.println(p.getLand().getName());
			System.out.println(p.getBerufe().size());
			for(Filmberuf fb : p.getBerufe()){
				System.out.println(fb.getName());
			}
			Land l = new Land();
			l.setId("123");
			l.setName("Ostdeutschland");
			l.setContraction("DDR");
			p.setVorname("Mirko der Held");
			p.setLand(l);
			session.save(l);
			session.save(p);
			
			
			tx.commit();
			
			
			
			
		} catch (Exception e){
			System.out.println("Error in getPersons " + e);
		}finally{
			System.out.println("mache das");
			if(session!=null){
				session.close();
				sessionFactory.close();
			}
			
		}
		return result;
	}
	
	
	private static SessionFactory getSessionFactory() {
		Configuration configuration = new Configuration().configure();
		StandardServiceRegistryBuilder builder = new StandardServiceRegistryBuilder()
				.applySettings(configuration.getProperties());
		SessionFactory sessionFactory = configuration.buildSessionFactory(builder.build());
		return sessionFactory;
	}
}
