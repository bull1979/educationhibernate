package biz.webgate.education.business;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="land")
public class Land {
	
	@Id
	private String id;
	private String name;
	private String contraction;
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	public String getContraction() {
		return contraction;
	}
	public void setContraction(String contraction) {
		this.contraction = contraction;
	}
	
	
	
	
}
