package biz.webgate.education.business;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name = "filmberuf")
public class Filmberuf {
	@Id
	private String id;
	
	@Column(name="name")
	private String name;
	
	@Transient
	public boolean help = false;
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
}
