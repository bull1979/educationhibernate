package biz.webgate.education.business;

import java.io.Serializable;
import java.util.Date;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "person")
public class Person implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public Person() {
	}

	@Id
	@Column(name = "id")
	private String id;

	@Column(name = "name")
	private String name;
	@Column(name = "vorname")
	private String vorname;
	@Column(name = "geburtstag")
	private Date geburtstag;

	@ManyToOne//show case(cascade = CascadeType.ALL) 
	@JoinColumn(name = "land")
	private Land land;

	@ManyToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	@JoinTable(name = "person_has_filmberuf", joinColumns = { 
			@JoinColumn(name = "Person_id", nullable = false, updatable = false) }, 
			inverseJoinColumns = { @JoinColumn(name = "Filmberuf_id", 
					nullable = false, updatable = false) })
	 private Set<Filmberuf> berufe;
	
	
	
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getVorname() {
		return vorname;
	}

	public void setVorname(String vorname) {
		this.vorname = vorname;
	}

	public Date getGeburtstag() {
		return geburtstag;
	}

	public void setD(Date geburtstag) {
		this.geburtstag = geburtstag;
	}

	public Land getLand() {
		return land;
	}

	public void setLand(Land land) {
		this.land = land;
	}

	public void setGeburtstag(Date geburtstag) {
		this.geburtstag = geburtstag;
	}

	public Set<Filmberuf> getBerufe() {
		return berufe;
	}

	public void setBerufe(Set<Filmberuf> berufe) {
		this.berufe = berufe;
	}
}
